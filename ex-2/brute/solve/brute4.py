#!/usr/bin/env python

import hashlib

def is_ok(number):
    res = hashlib.md5(str(number)).hexdigest()
    res = res[0:32]
    if res[0] != "0" or  res[1] != "e":
	return False
    for i in range(2,32):
	if res[i] > "9"  or  res[i] <"0":
	    return False
    return True


for i in range(1,10000000):
    if is_ok(i):
	print i
	break 



